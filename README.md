# iObjects Java Function Test

## 1. 介绍
&emsp;&emsp;SuperMap iObjects Java 组件功能测试

## 2. 功能测试列表

- 打开工作空间
- 空间查询
- S3M 转 3DTiles
- TIN 地形缓存生成
- 点云缓存生成
- 矢量数据集清空记录集
- 三维空间查询
- 模型生成缓存
- 地图生成缓存