package com.company;

import com.company.helper.Helper;
import com.supermap.data.*;
import com.supermap.data.processing.CacheBuilderTIN;
import com.supermap.data.processing.StorageType;
import com.supermap.data.processing.TilingSchemeOption;

public class CacheBuilderTINTest {

    private static final String datasourceFilePath = "D:\\SuperMap\\supermap-iobjectsjava-11.0.1-21420-98023-win-all\\SampleData\\BeijingDEM\\BeijingDEM.udb";
    private static final String tinCacheName = "TINCache";

    private static final SteppedListener steppedListener = steppedEvent -> {
        System.out.println("TIN 地形缓存生成进度：" + steppedEvent.getPercent() + " ---- 剩余时间：" + Helper.getDate(steppedEvent.getRemainTime()));
    };

    public static void main(String[] args) {
        String runtimeFolderPath = System.getProperty("user.dir");
        String outputCacheFolderPath = runtimeFolderPath + "\\out";

        Helper.deleteFolder(outputCacheFolderPath + "\\" + tinCacheName);

        DatasourceConnectionInfo datasourceConnectionInfo = new DatasourceConnectionInfo();
        datasourceConnectionInfo.setServer(datasourceFilePath);
        datasourceConnectionInfo.setEngineType(EngineType.UDB);
        datasourceConnectionInfo.setAlias("BeijingDEM");

        Workspace workspace = new Workspace();
        Datasources datasources = workspace.getDatasources();
        Datasource datasource = datasources.open(datasourceConnectionInfo);
        DatasetGrid datasetGrid = (DatasetGrid) datasource.getDatasets().get("DEM25");

        CacheBuilderTIN cacheBuilderTIN = new CacheBuilderTIN();
        cacheBuilderTIN.setCacheName(tinCacheName);
        cacheBuilderTIN.setDataset(datasetGrid);
        cacheBuilderTIN.setStoreType(StorageType.Original);
        cacheBuilderTIN.setCompressed(false);
        cacheBuilderTIN.setOutputFolder(outputCacheFolderPath);
        cacheBuilderTIN.setProcessThreadsCount(12);
        cacheBuilderTIN.setBounds(datasetGrid.getBounds());
        cacheBuilderTIN.setEncodeDXT(true);
        cacheBuilderTIN.setTilingScheme(TilingSchemeOption.GEOGRAPHICTILINGSCHEME);
        cacheBuilderTIN.setDealtOfEndLevel(38);
//        cacheBuilderTIN.setBeginLevel(6);
//        cacheBuilderTIN.setEndLevel(8);

        cacheBuilderTIN.addSteppedListener(steppedListener);

        boolean result = false;

        try {
            result = cacheBuilderTIN.buildTIN();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            datasetGrid.close();
            datasetGrid.dispose();

            datasource.close();
            datasourceConnectionInfo.dispose();

            workspace.close();
            workspace.dispose();

            cacheBuilderTIN.removeSteppedListener(steppedListener);
            cacheBuilderTIN.dispose();
        }

        System.out.println(result ? "地形缓存生成成功！" : "地形缓存生成失败！！！");
    }
}
