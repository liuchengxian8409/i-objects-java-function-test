package com.company;

import com.supermap.data.GeoPoint3D;
import com.supermap.data.Geometry;
import com.supermap.data.Geometry3D;
import com.supermap.realspace.threeddesigner.CacheProcessor3D;

import java.util.ArrayList;

public class CacheProcessor3DTest {

    private static String scpFilePath = "D:\\SuperMap\\SuperMap SampleData\\倾斜摄影丽江10G\\Config\\Combine.scp";

    public static void main(String[] args) {

        ArrayList<Geometry> geometryList = new ArrayList<>();
        GeoPoint3D geoPoint3D = new GeoPoint3D();
        geoPoint3D.setX(100.4099);
        geoPoint3D.setY(27.4243);
        geoPoint3D.setZ(0);
        geometryList.add(geoPoint3D);

        long startTime = System.currentTimeMillis();

        ArrayList<Geometry3D> geometry3DS = CacheProcessor3D.geometryMatch(scpFilePath, geometryList);

        long endTime = System.currentTimeMillis();

        System.out.println("耗时：" + (endTime - startTime) + " ms");
        System.out.println(((GeoPoint3D)geometry3DS.get(0)).getZ());
    }
}
