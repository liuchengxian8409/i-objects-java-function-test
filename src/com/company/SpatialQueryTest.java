package com.company;

import com.supermap.data.*;

public class SpatialQueryTest {

    private static final String datasourceFilePath = "D:\\SuperMap\\supermap-iobjectsjava-11.0.1-21420-98023-win-all\\SampleData\\World\\world.udbx";

    private static final String yukonServer = "172.16.15.144:5432";
    private static final String yukonDatabase = "supermap";
    private static final String yukonUser = "supermap";
    private static final String yukonPassword = "SuperMap@123";

    public static void main(String[] args) {

        Workspace workspace = new Workspace();
        Datasources datasources = workspace.getDatasources();

        DatasourceConnectionInfo datasourceConnectionInfo = null;
//        datasourceConnectionInfo = getDatasourceConnectionInfo(EngineType.UDBX);
        datasourceConnectionInfo = getDatasourceConnectionInfo(EngineType.YUKON);

        Datasource datasource = datasources.open(datasourceConnectionInfo);
        DatasetVector datasetVector = (DatasetVector) datasource.getDatasets().get("World");
        int recordsetCount = queryObject(datasetVector);

        System.out.println("空间查询结果：共计返回 " + recordsetCount + " 条记录");
    }

    private static DatasourceConnectionInfo getDatasourceConnectionInfo(EngineType engineType) {
        DatasourceConnectionInfo datasourceConnectionInfo = new DatasourceConnectionInfo();

        if (engineType == EngineType.UDBX) {
            datasourceConnectionInfo.setEngineType(EngineType.UDBX);
            datasourceConnectionInfo.setServer(datasourceFilePath);
            datasourceConnectionInfo.setAlias("World");
        } else if (engineType == EngineType.YUKON) {
            datasourceConnectionInfo.setEngineType(EngineType.YUKON);
            datasourceConnectionInfo.setServer(yukonServer);
            datasourceConnectionInfo.setDatabase(yukonDatabase);
            datasourceConnectionInfo.setUser(yukonUser);
            datasourceConnectionInfo.setPassword(yukonPassword);
        }

        return datasourceConnectionInfo;
    }

    private static int queryObject(DatasetVector datasetVector) {
        GeoRegion geoRegion = new GeoRegion();
        Point2Ds point2Ds = new Point2Ds();
        Point2D point2D = new Point2D(106.3265, 29.393242);
        point2Ds.add(point2D);
        point2D = new Point2D(106.4695, 29.393242);
        point2Ds.add(point2D);
        point2D = new Point2D(106.63254, 29.36541);
        point2Ds.add(point2D);
        point2D = new Point2D(106.12648, 29.8624541);
        point2Ds.add(point2D);
        point2D = new Point2D(106.3265, 29.393242);
        point2Ds.add(point2D);
        geoRegion.addPart(point2Ds);

        QueryParameter queryParameter = new QueryParameter();
        queryParameter.setSpatialQueryMode(SpatialQueryMode.INTERSECT);
        queryParameter.setSpatialQueryObject(geoRegion);

        int recordsetCount = 0;
        Recordset recordset = null;
        try {
            recordset = datasetVector.query(queryParameter);
            recordsetCount = recordset.getRecordCount();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (recordset != null) {
                recordset.close();
                recordset.dispose();
            }
        }

        return recordsetCount;
    }
}
