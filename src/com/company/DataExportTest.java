package com.company;

import com.supermap.data.*;
import com.supermap.data.conversion.*;

public class DataExportTest {
    public static void main(String[] args) {
        String datasourceFilePath = "C:\\Users\\LiuChengxian\\Downloads\\DataSource.udbx";
        DatasourceConnectionInfo connectionInfo = new DatasourceConnectionInfo();
        connectionInfo.setServer(datasourceFilePath);
        connectionInfo.setEngineType(EngineType.UDBX);
        connectionInfo.setAlias("test");
        Workspace workspace = new Workspace();
        Datasource datasource = workspace.getDatasources().open(connectionInfo);
        Dataset[] datasets = new Dataset[datasource.getDatasets().getCount()];
        Datasets datasets1 = datasource.getDatasets();
        for (int i = 0; i < datasets1.getCount(); i++) {
            datasets[i] = datasets1.get(i);
        }

        String targetFilePath = "C:\\Users\\LiuChengxian\\Downloads\\export.mdb";
        ExportSettingPersonalGDBVector settingPersonalGDBVector = null;
        // 点、线、面的DatasetVector
        settingPersonalGDBVector = new ExportSettingPersonalGDBVector(datasets, targetFilePath);
        // 重新设定一下路径，也是不可以执行
        settingPersonalGDBVector.setTargetFilePath("C:\\Users\\LiuChengxian\\Downloads\\export.mdb");
        settingPersonalGDBVector.setTargetFileCharset(Charset.UTF8);
        settingPersonalGDBVector.setTargetFileType(FileType.PersonalGDBVector);
        settingPersonalGDBVector.setOverwrite(true);
        settingPersonalGDBVector.setTargetFileCharset(Charset.UTF8);

        DataExport dataExport = new DataExport();
        // 导出设置信息集合类
        ExportSettings exportSettings = dataExport.getExportSettings();
        // 向导出设置信息集合中添加一个导出设置信息对象
        exportSettings.add(settingPersonalGDBVector);
        dataExport.setExportSettings(exportSettings);
        // 导出结果类
        ExportResult run = dataExport.run();
        if (run.getSucceedSettings().length > 0 && run.getFailedSettings().length == 0) {
            System.out.println("导出 mdb 成功！");
        }
    }
}
