package com.company;

import com.supermap.analyst.spatialanalyst.OverlayAnalyst;
import com.supermap.analyst.spatialanalyst.OverlayAnalystParameter;
import com.supermap.data.*;

public class OverlayAnalystTest {

    private static final String datasourceFilePath = "E:\\SuperMap\\数据\\客户数据\\矢量\\YDHX.udbx";

    public static void main(String[] args) {
        DatasourceConnectionInfo datasourceConnectionInfo = new DatasourceConnectionInfo();
        datasourceConnectionInfo.setEngineType(EngineType.UDBX);
        datasourceConnectionInfo.setServer(datasourceFilePath);

        Workspace workspace = new Workspace();
        Datasources datasources = workspace.getDatasources();
        Datasource datasource = datasources.open(datasourceConnectionInfo);
        Datasets datasets = datasource.getDatasets();
        DatasetVector srcDatasetVector = (DatasetVector) datasets.get(0);
        DatasetVector overlayDatasetVector = (DatasetVector) datasets.get(1);

        DatasetVectorInfo datasetVectorInfo = new DatasetVectorInfo();
        datasetVectorInfo.setName(datasets.getAvailableDatasetName("OverlayResult"));
        datasetVectorInfo.setType(DatasetType.REGION);
        DatasetVector retDatasetVector = datasets.create(datasetVectorInfo);
        retDatasetVector.setPrjCoordSys(srcDatasetVector.getPrjCoordSys().clone());

        OverlayAnalystParameter overlayAnalystParameter = new OverlayAnalystParameter();
        overlayAnalystParameter.setOperationRetainedFields(new String[]{"SmUserId"});
        overlayAnalystParameter.setSourceRetainedFields(new String[]{"SmUserId"});
        overlayAnalystParameter.setSingleResult(false);
        overlayAnalystParameter.setTolerance(0.0000011074);

        boolean ret = OverlayAnalyst.intersect(srcDatasetVector, overlayDatasetVector, retDatasetVector, overlayAnalystParameter);
        System.out.println(ret ? "叠加分析成功！" : "叠加分析失败！！！");
        System.out.println("叠加分析结果数据集记录数：" + retDatasetVector.getRecordCount());
    }
}
