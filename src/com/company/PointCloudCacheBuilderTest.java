package com.company;

import com.company.helper.Helper;
import com.supermap.data.*;
import com.supermap.data.processing.*;
import com.supermap.realspace.CacheFileType;

import java.util.ArrayList;
import java.util.HashMap;

public class PointCloudCacheBuilderTest {

    private static final String pointCloudFilePath = "D:\\SuperMap\\supermap-iobjectsjava-11.0.1-21420-98023-win-all\\SampleData\\PointClound\\181115_071549_0.las";
    private static final String pointCloudCacheName = "PointCloudCache";

    private static final SteppedListener steppedListener = steppedEvent -> {
        System.out.println("点云缓存生成进度：" + steppedEvent.getPercent() + " ---- 剩余时间：" + Helper.getDate(steppedEvent.getRemainTime()));
    };

    public static void main(String[] args) {

        String runtimeFolderPath = System.getProperty("user.dir");
        String outputCacheFolderPath = runtimeFolderPath + "\\out";
        String pointCloudCachePath = outputCacheFolderPath + "\\" + pointCloudCacheName;

        Helper.deleteFolder(pointCloudCachePath);

        int[] validClassifyInfos = PointCloudCacheBuilder.getValidClassifyInfos(pointCloudFilePath);

        PointCloudFileInfo pointCloudFileInfo = new PointCloudFileInfo();
        pointCloudFileInfo.setPointInfoType(PointCloudInfoType.XYZ);
        pointCloudFileInfo.setArrClassify(validClassifyInfos);
        pointCloudFileInfo.setPointDataRGBMode(DataRGBMode.RGB_0_255);
        pointCloudFileInfo.setPosition(new Point3D(0, 0, 0));
        HashMap<String, ArrayList<String>> groupFileNames = new HashMap<>();
        ArrayList<String> filePaths = new ArrayList<>();
        filePaths.add(pointCloudFilePath);
        groupFileNames.put("group", filePaths);
        pointCloudFileInfo.setGroupFileNames(groupFileNames);

        PointCloudCacheBuilder pointCloudCacheBuilder = new PointCloudCacheBuilder();
        pointCloudCacheBuilder.addSteppedListener(steppedListener);
        pointCloudCacheBuilder.setProcessType(ProcessFileType.ADD);
        pointCloudCacheBuilder.setCacheVersion(S3MVersion.VERSION_30);
        pointCloudCacheBuilder.setFileType(CacheFileType.S3MB);
        pointCloudCacheBuilder.setGenerateNormal(true);
        pointCloudCacheBuilder.setGeometryCompressType(MeshCompressType.MESHOPT);
        pointCloudCacheBuilder.setTilePyramidSplitType(PyramidSplitType.Octree);
        pointCloudCacheBuilder.setOutputFolder(outputCacheFolderPath);
        pointCloudCacheBuilder.setCacheName(pointCloudCacheName);
        pointCloudCacheBuilder.setPointCloudInfos(pointCloudFileInfo);
        pointCloudCacheBuilder.setCategoryField(PointCloudCategoryField.POSZ);

        boolean built = false;
        try {
            built = pointCloudCacheBuilder.build();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pointCloudCacheBuilder.removeSteppedListener(steppedListener);
            pointCloudCacheBuilder.dispose();
        }

        System.out.println(built ? "生成点云缓存成功！" : "生成点云缓存失败！！！");
    }
}
