package com.company;

import com.supermap.data.Workspace;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.data.WorkspaceType;
import com.supermap.data.WorkspaceVersion;

public class WorkspaceTest {

    private static final String workspaceFilePath = "D:\\SuperMap\\supermap-iobjectsjava-11.0.1-21420-98023-win-all\\SampleData\\World\\World.smwu";

    public static void main(String[] args) {

        WorkspaceConnectionInfo workspaceConnectionInfo = new WorkspaceConnectionInfo();
        workspaceConnectionInfo.setServer(workspaceFilePath);
        workspaceConnectionInfo.setType(WorkspaceType.SMWU);
        workspaceConnectionInfo.setVersion(WorkspaceVersion.UGC70);
        workspaceConnectionInfo.setName("Workspace");

        Workspace workspace = new Workspace();

        try {
            boolean opened = workspace.open(workspaceConnectionInfo);
            System.out.println(opened ? "打开工作空间成功！" : "打开工作空间失败！！！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            workspaceConnectionInfo.dispose();
            workspace.close();
            workspace.dispose();
        }
    }
}
