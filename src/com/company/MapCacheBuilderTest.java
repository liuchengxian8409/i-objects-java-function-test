package com.company;

import com.company.helper.Helper;
import com.supermap.data.*;
import com.supermap.data.processing.*;
import com.supermap.mapping.Map;

import java.util.HashMap;

public class MapCacheBuilderTest {

    private static final String workspaceFilePath = "D:\\SuperMap\\supermap-iobjectsjava-11.0.1-21420-98023-win-all\\SampleData\\World\\World.smwu";
    private static final String mvtCacheName = "MapCache";

    private static final SteppedListener steppedListener = steppedEvent -> {
        System.out.println("MVT 矢量瓦片生成进度：" + steppedEvent.getPercent() + " ---- 剩余时间：" + Helper.getDate(steppedEvent.getRemainTime()));
    };

    public static void main(String[] args) {
        String runtimeFolderPath = System.getProperty("user.dir");
        String outputCacheFolderPath = runtimeFolderPath + "\\out";

        Helper.deleteFolder(outputCacheFolderPath + "\\" + mvtCacheName);

        WorkspaceConnectionInfo workspaceConnectionInfo = new WorkspaceConnectionInfo();
        workspaceConnectionInfo.setType(WorkspaceType.SMWU);
        workspaceConnectionInfo.setVersion(WorkspaceVersion.UGC70);
        workspaceConnectionInfo.setServer(workspaceFilePath);

        Workspace workspace = new Workspace();
        if (workspace.open(workspaceConnectionInfo)) {
            String mapXML = workspace.getMaps().getMapXML("testmap");
            Map map = new Map();
            map.setWorkspace(workspace);
            map.fromXML(mapXML, WorkspaceVersion.UGC70);

            MapCacheBuilder mapCacheBuilder = new MapCacheBuilder();
            mapCacheBuilder.addSteppedListener(steppedListener);
            mapCacheBuilder.setMap(map);
            mapCacheBuilder.setBounds(map.getBounds());
            mapCacheBuilder.setIndexBounds(mapCacheBuilder.getGlobalIndexBounds());
            mapCacheBuilder.setStorageType(StorageType.Original);
            mapCacheBuilder.setOutputFolder(outputCacheFolderPath);
            mapCacheBuilder.setCacheName(mvtCacheName);
            mapCacheBuilder.setTileFormat(TileFormat.PBF);
//            mapCacheBuilder.setTileSize(TileSize.SIZE256);
            mapCacheBuilder.setMVTTileBuffer(128);
            mapCacheBuilder.setMVTSimplifyGeometry(true);
            mapCacheBuilder.setMVTStyleWithoutFont(false);
            mapCacheBuilder.setMVTWithAllField(true);
            mapCacheBuilder.setTilingMode(MapTilingMode.GLOBAL);

            HashMap<Double, String> scalesMaps = mapCacheBuilder.getGlobalLevelScales();
            Integer minScales = 0;
            Integer maxScales = 5;
            double[] outputScales = getOutputScales(scalesMaps, minScales, maxScales);
            HashMap<Double, String> doubleStringHashMap2 = new HashMap<>();
            doubleStringHashMap2.put(outputScales[0], "0");
            doubleStringHashMap2.put(outputScales[1], "1");
            doubleStringHashMap2.put(outputScales[2], "2");
            doubleStringHashMap2.put(outputScales[3], "3");
            doubleStringHashMap2.put(outputScales[4], "4");
            doubleStringHashMap2.put(outputScales[5], "5");
            mapCacheBuilder.setOutputScales(outputScales);
            mapCacheBuilder.setOutputScaleCaptions(doubleStringHashMap2);
            mapCacheBuilder.resumable(false);

            boolean built = false;
            try {
                built = mapCacheBuilder.build();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                System.out.println(built ? "MVT 矢量瓦片生成成功！" : "MVT 矢量瓦片生成失败！！！");

                mapCacheBuilder.removeSteppedListener(steppedListener);
                mapCacheBuilder.dispose();

                map.close();
                map.dispose();
            }
        }

        workspace.close();
        workspace.dispose();
    }

    private static double[] getOutputScales(HashMap<Double, String> scalesMaps, Integer minScales, Integer maxScales) {
        int arraylength = maxScales - minScales + 1;
        double[] outputScales = new double[arraylength];
        String[] values = new String[arraylength];
        for (int i = 0; i < values.length; i++) {
            values[i] = String.valueOf(minScales + i);
        }
        for (int i = 0; i < outputScales.length; i++) {
            outputScales[i] = getMapKey(scalesMaps, values[i]);
        }
        return outputScales;
    }

    private static Double getMapKey(HashMap<Double, String> maps, String value) {
        Double key = 0.0;
        for (java.util.Map.Entry<Double, String> entry : maps.entrySet()) {
            if (entry.getValue().equals(value)) {
                key = entry.getKey();
            }
        }
        return key;
    }

    private static void updateMapCache(Workspace workspace, String updateMapName, String sciConfigFilePath, String outputFolder, Rectangle2D updateBounds) {
        Map map = new Map(workspace);
        Maps maps = workspace.getMaps();
        map.fromXML(maps.getMapXML(updateMapName));

        MapCacheBuilder mapCacheBuilder = new MapCacheBuilder();
        mapCacheBuilder.addSteppedListener(steppedEvent -> System.out.println(steppedEvent.getPercent() + " %"));
        mapCacheBuilder.fromConfigFile(sciConfigFilePath);
        mapCacheBuilder.setMap(map);
        mapCacheBuilder.setIsAppending(true);
        mapCacheBuilder.setBounds(updateBounds);
        mapCacheBuilder.setOutputFolder(outputFolder);
        mapCacheBuilder.setFillMargin(true);
        boolean build = mapCacheBuilder.build();
        System.out.println(build);
    }
}
