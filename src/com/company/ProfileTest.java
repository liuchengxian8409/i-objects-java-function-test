package com.company;

import com.supermap.data.*;
import com.supermap.realspace.Scene;
import com.supermap.realspace.SceneType;
import com.supermap.realspace.spatialanalyst.Profile;
import com.supermap.ui.Action3D;
import com.supermap.ui.SceneControl;
import com.supermap.ui.Tracked3DListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ProfileTest {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(
                        UIManager.getSystemLookAndFeelClassName());
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            MainFrame mainFrame = new MainFrame();
            mainFrame.setVisible(true);
        });
    }
}

class MainFrame extends JFrame {

    private JMenuItem loadSceneMenuItem;
    private JMenuItem profileMenuItem;
    private JMenuItem exportMenuItem;

    private SceneControl sceneControl;
    private Workspace workspace;
    private Profile profile;

    private final Tracked3DListener tracked3DListener = tracked3DEvent -> {
        if (sceneControl.getAction() == Action3D.CREATE_LINE) {
            Geometry3D geometry = tracked3DEvent.getGeometry();
            GeoLine3D geoLine3D = (GeoLine3D) geometry;
            Point3Ds point3Ds = geoLine3D.getPart(0);
            if (point3Ds.getCount() >= 2) {
                sceneControl.setAction(Action3D.SELECT);

                profile.setStartPoint(point3Ds.getItem(0));
                profile.setEndPoint(point3Ds.getItem(1));
                profile.setExtendHeight(100);

                profileMenuItem.setEnabled(false);
                exportMenuItem.setEnabled(true);
            }
        }
    };

    public MainFrame() {
        Environment.setSceneAntialias(true);
        Environment.setOpenCLComputingEnabled(true);
        Environment.setCUDAComputingEnabled(true);
        Environment.setHardwareAccelerationEnable(true);
        Environment.setCurrentCulture("zh-CN");

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(1100, 800);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((screenSize.getWidth() - this.getWidth()) / 2);
        int y = (int) ((screenSize.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);
        this.setTitle("剖面分析功能测试");

        JPanel mainPanel = (JPanel) getContentPane();
        mainPanel.setLayout(new BorderLayout());
        sceneControl = new SceneControl(SceneType.GLOBE);
        add(sceneControl, BorderLayout.CENTER);

        initMenu();
    }

    private void initMenu() {
        JPanel mainPanel = (JPanel) getContentPane();

        JMenuBar menuBar = new JMenuBar();
        mainPanel.add(menuBar, BorderLayout.NORTH);
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        menuBar.setBounds(0, 0, getWidth(), 30);

        loadSceneMenuItem = new JMenuItem("加载场景数据");
        loadSceneMenuItem.setEnabled(true);
        menuBar.add(loadSceneMenuItem);
        loadSceneMenuItem.addActionListener(e -> {
            WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo();
            connectionInfo.setServer("D:\\SuperMap\\SuperMap SampleData\\3D\\CBDDataset\\CBD.smwu");
            connectionInfo.setType(WorkspaceType.SMWU);
            workspace = new Workspace();
            workspace.open(connectionInfo);

            Scene scene = sceneControl.getScene();
            scene.setWorkspace(workspace);
            scene.open(workspace.getScenes().get(0));
            sceneControl.addTrackedListener(tracked3DListener);

            loadSceneMenuItem.setEnabled(false);
            profileMenuItem.setEnabled(true);
            exportMenuItem.setEnabled(false);
        });

        profileMenuItem = new JMenuItem("剖面分析");
        menuBar.add(profileMenuItem);
        profileMenuItem.setEnabled(false);
        profileMenuItem.addActionListener((e) -> {
            if (profile == null) {
                profile = new Profile(sceneControl.getScene());
                profile.build();
            }

            sceneControl.setAction(Action3D.CREATE_LINE);
            profileMenuItem.setEnabled(false);
        });

        exportMenuItem = new JMenuItem("显示剖面分析结果");
        menuBar.add(exportMenuItem);
        exportMenuItem.setEnabled(false);
        exportMenuItem.addActionListener((e) -> {
            BufferedImage bufferedImage = profile.outputProfileToBitMap();
//            File outputFile = new File("profile.png");
            try {
//                ImageIO.write(bufferedImage, "png", outputFile);

                JDialog outputDialog = new JDialog();
                outputDialog.setTitle("剖面分析结果");
                outputDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                outputDialog.setSize(bufferedImage.getWidth(), bufferedImage.getHeight());
                JLabel pLbl = new JLabel(new ImageIcon(bufferedImage));
                outputDialog.add(pLbl);

                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int x = (int) ((screenSize.getWidth() - outputDialog.getWidth()) / 2);
                int y = (int) ((screenSize.getHeight() - outputDialog.getHeight()) / 2);
                outputDialog.setLocation(x, y);

                outputDialog.setVisible(true);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                loadSceneMenuItem.setEnabled(true);
                profileMenuItem.setEnabled(false);
                exportMenuItem.setEnabled(false);
            }
        });
    }
}
