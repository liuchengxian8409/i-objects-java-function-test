package com.company;

import com.supermap.data.Datasource;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.data.EngineType;
import com.supermap.data.Workspace;
import com.supermap.data.conversion.DataImport;
import com.supermap.data.conversion.ImportMode;
import com.supermap.data.conversion.ImportResult;
import com.supermap.data.conversion.ImportSettingSHP;

import java.io.File;

public class DataImportTest {

    public static void main(String[] args) {
        String shpFilePath = args[0];
        String dmServer = args[1];
        String dmUser = args[2];
        String dmPassword = args[3];

        File file = new File(shpFilePath);
        String name = file.getName();
        String[] splits = name.split("\\.");
        String alias = splits[0];

        DatasourceConnectionInfo connectionInfo = new DatasourceConnectionInfo();
        connectionInfo.setEngineType(EngineType.DM);
        connectionInfo.setAlias(alias);
        connectionInfo.setServer(dmServer);
        connectionInfo.setUser(dmUser);
        connectionInfo.setPassword(dmPassword);

        Workspace workspace = new Workspace();
        Datasource datasource = workspace.getDatasources().open(connectionInfo);

        ImportSettingSHP importSettingSHP = new ImportSettingSHP();
        importSettingSHP.setSourceFilePath(shpFilePath);
        importSettingSHP.setTargetDatasource(datasource);
        importSettingSHP.setImportMode(ImportMode.OVERWRITE);
        importSettingSHP.setAttributeIgnored(false);
        importSettingSHP.setImporttingAs3D(false);
        importSettingSHP.setTargetDatasetName(datasource.getDatasets().getAvailableDatasetName(alias));

        DataImport dataImport = new DataImport();
        dataImport.getImportSettings().add(importSettingSHP);
        dataImport.addImportSteppedListener(importSteppedEvent -> {
            System.out.println("导入 Shp 进度：" + importSteppedEvent.getTotalPercent());
        });
        ImportResult importResult = dataImport.run();
        if (importResult.getFailedSettings().length == 0 && importResult.getSucceedSettings().length > 0) {
            System.out.println("导入 Shp 成功！");
        } else {
            System.out.println("导入 Shp 失败！！！");
        }

        importSettingSHP.dispose();
        dataImport.dispose();
        datasource.close();
        workspace.close();
        workspace.dispose();
    }
}
