package com.company;

import com.supermap.data.*;

public class TruncateTest {

    private static final String server = "172.16.12.186:5432";
    private static final String database = "testlcxpgis";
    private static final String user = "postgres";
    private static final String password = "123654";

    public static void main(String[] args) {

        DatasourceConnectionInfo datasourceConnectionInfo = new DatasourceConnectionInfo();
        datasourceConnectionInfo.setServer(server);
        datasourceConnectionInfo.setDatabase(database);
        datasourceConnectionInfo.setUser(user);
        datasourceConnectionInfo.setPassword(password);
        datasourceConnectionInfo.setEngineType(EngineType.PGGIS);
        datasourceConnectionInfo.setAlias("PGGIS");

        Workspace workspace = new Workspace();
        Datasource datasource = workspace.getDatasources().open(datasourceConnectionInfo);
        DatasetVector datasetVector = (DatasetVector) datasource.getDatasets().get("World");
        boolean truncated = false;

        try {
            truncated = datasetVector.truncate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            datasetVector.close();
            datasetVector.dispose();

            datasource.close();
            datasourceConnectionInfo.dispose();

            workspace.close();
            workspace.dispose();
        }

        System.out.println(truncated ? "清除数据集记录成功" : "清除数据集记录失败！");
    }
}
