package com.company;

import com.company.helper.Helper;
import com.supermap.data.*;
import com.supermap.data.processing.CacheImageType;
import com.supermap.data.processing.ImageCacheBuilder;
import com.supermap.data.processing.ImageTilingMode;
import com.supermap.data.processing.StorageType;

public class ImageCacheBuilderTest {

    private static final String workspaceFilePath = "C:\\Users\\LiuChengxian\\Downloads\\wenjian.smwu";
    private static final String imageCacheName = "ImageCache";
    private static Workspace workspace;

    public static void main(String[] args) {

        WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo();
        connectionInfo.setType(WorkspaceType.SMWU);
        connectionInfo.setServer(workspaceFilePath);

        workspace = new Workspace();
        if (workspace.open(connectionInfo)) {
            String runtimeFolderPath = System.getProperty("user.dir");
            String outputCacheFolderPath = runtimeFolderPath + "\\out";

            Helper.deleteFolder(outputCacheFolderPath + "\\" + imageCacheName);

//            buildSci3dCache();
            updateSci3dCache(imageCacheName);
        }
    }

    public static void buildSci3dCache() {
        String runtimeFolderPath = System.getProperty("user.dir");
        String outputCacheFolderPath = runtimeFolderPath + "\\out";

        Datasources datasources = workspace.getDatasources();
        Datasource datasource = datasources.get(0);
        Datasets datasets = datasource.getDatasets();

        ImageCacheBuilder imageCacheBuilder = new ImageCacheBuilder();
        imageCacheBuilder.setOutputFolder(outputCacheFolderPath);
        imageCacheBuilder.setCacheName(imageCacheName);
        imageCacheBuilder.setImageType(CacheImageType.PNG);
        imageCacheBuilder.setTilingMode(ImageTilingMode.GLOBAL);
        imageCacheBuilder.setStorageType(StorageType.Original);
        imageCacheBuilder.setDataset((DatasetImage) datasets.get("RS3D_IMG_3857"));
        imageCacheBuilder.setBeginLevel(10);
        imageCacheBuilder.setEndLevel(12);
        imageCacheBuilder.setProcessThreadsCount(12);

        imageCacheBuilder.computeOutputScales();

        try {
            imageCacheBuilder.addSteppedListener(steppedEvent -> {
                System.out.println(steppedEvent.getPercent() + " %");
            });

            if (imageCacheBuilder.build()) {
                System.out.println("影像缓存 sci3d 生成成功！");
            } else {
                System.out.println("影像缓存 sci3d 生成失败！！！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            imageCacheBuilder.dispose();
        }
    }

    public static void updateSci3dCache(String imageCacheName) {
        String runtimeFolderPath = System.getProperty("user.dir");
        String outputCacheFolderPath = runtimeFolderPath + "\\out";
        String imageCacheSci3dFilePath = outputCacheFolderPath + "\\" + imageCacheName;

        outputCacheFolderPath = "C:\\Users\\LiuChengxian\\Downloads";
        imageCacheName = "投影后的地图1";
        imageCacheSci3dFilePath = "C:\\Users\\LiuChengxian\\Downloads\\投影后的地图1";
//        imageCacheName = "RS3D_IMG_1@TempDatasource";
//        imageCacheSci3dFilePath = "C:\\Users\\LiuChengxian\\Downloads\\RS3D_IMG_1@TempDatasource\\RS3D_IMG_1@TempDatasource.sci3d";

        Datasources datasources = workspace.getDatasources();
        Datasource datasource = datasources.get(0);
        Datasets datasets = datasource.getDatasets();

        ImageCacheBuilder imageCacheBuilder = new ImageCacheBuilder();
        imageCacheBuilder.fromConfigFile(imageCacheSci3dFilePath);
        imageCacheBuilder.setOutputFolder(outputCacheFolderPath);
        imageCacheBuilder.setCacheName(imageCacheName);
        imageCacheBuilder.setDataset((DatasetImage) datasets.get("影像下载_2004091527"));

        DatasetVector newRegion = (DatasetVector) datasets.get("NewRegion_3857");
        Recordset recordset = newRegion.getRecordset(false, CursorType.STATIC);
        recordset.moveFirst();
        GeoRegion geometry = (GeoRegion) recordset.getGeometry();

        imageCacheBuilder.setUpdateRegion(geometry);

        imageCacheBuilder.addSteppedListener(steppedEvent -> System.out.println(steppedEvent.getPercent() + "    " + steppedEvent.getRemainTime()));

        try {
            if (imageCacheBuilder.build()) {
                System.out.println("影像缓存 sci3d 更新成功！");
            } else {
                System.out.println("影像缓存 sci3d 更新失败！！！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            imageCacheBuilder.dispose();
        }
    }
}
