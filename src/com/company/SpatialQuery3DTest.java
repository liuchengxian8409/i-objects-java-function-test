package com.company;

import com.supermap.data.*;
import com.supermap.realspace.threeddesigner.PositionMode;
import com.supermap.realspace.threeddesigner.SpatialQuery3D;

import java.util.Arrays;

public class SpatialQuery3DTest {

    private static final String datasourceFilePath = "D:\\SuperMap\\supermap-iobjectsjava-11.0.1-21420-98023-win-all\\SampleData\\Model3D\\model3d.udbx";

    public static void main(String[] args) {

        DatasourceConnectionInfo datasourceConnectionInfo = new DatasourceConnectionInfo();
        datasourceConnectionInfo.setEngineType(EngineType.UDBX);
        datasourceConnectionInfo.setServer(datasourceFilePath);
        datasourceConnectionInfo.setAlias("model3d");

        Workspace workspace = new Workspace();
        Datasources datasources = workspace.getDatasources();
        Datasource datasource = datasources.open(datasourceConnectionInfo);
        Datasets datasets = datasource.getDatasets();
        DatasetVector building3dDatasetVector = (DatasetVector) datasets.get("building3d");
        DatasetVector test3dDatasetVector = (DatasetVector) datasets.get("test3d");

        Recordset test3dRecordset = test3dDatasetVector.getRecordset(false, CursorType.STATIC);
        Geometry3D geometry3d = (Geometry3D) test3dRecordset.getGeometry();
        Recordset building3dRecordset = building3dDatasetVector.getRecordset(false, CursorType.STATIC);

        int[] smIds = new int[0];
        try {
            smIds = SpatialQuery3D.spatialQuery(geometry3d, building3dRecordset, PositionMode.WITHIN);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            building3dRecordset.close();
            building3dRecordset.dispose();
            test3dRecordset.close();
            test3dRecordset.dispose();

            building3dDatasetVector.close();
            building3dDatasetVector.dispose();

            test3dDatasetVector.close();
            test3dDatasetVector.dispose();

            datasource.close();

            workspace.close();
            workspace.dispose();
        }

        System.out.println(Arrays.toString(smIds));
    }
}
