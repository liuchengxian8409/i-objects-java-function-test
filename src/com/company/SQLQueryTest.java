package com.company;

import com.supermap.data.*;

public class SQLQueryTest {

    private static final String server = "192.168.160.130:5432";
    private static final String userName = "supermap";
    private static final String password = "SuperMap@123";
    private static final String databaseName = "supermap";

    public static void main(String[] args) {
        DatasourceConnectionInfo connectionInfo = new DatasourceConnectionInfo();
        connectionInfo.setEngineType(EngineType.YUKON);
        connectionInfo.setServer(server);
        connectionInfo.setUser(userName);
        connectionInfo.setPassword(password);
        connectionInfo.setDatabase(databaseName);
        connectionInfo.setAlias("testdb");

        Workspace workspace = new Workspace();
        Datasource datasource = null;
        DatasetVector datasetVector = null;
        Recordset recordset = null;
        QueryParameter queryParameter = new QueryParameter();

        try {
            Datasources datasources = workspace.getDatasources();
            datasource = datasources.open(connectionInfo);
            Datasets datasets = datasource.getDatasets();
            datasetVector = (DatasetVector) datasets.get(0);

            queryParameter.setResultFields(new String[]{"*"});
            queryParameter.setAttributeFilter("SmID <= 20");
            recordset = datasetVector.query(queryParameter);
            recordset.moveFirst();
            while (recordset.moveNext()) {
                Object country_en = recordset.getFieldValue("country_en");
                System.out.println(country_en);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (recordset != null) {
                recordset.close();
                recordset.dispose();
            }

            queryParameter.dispose();

            if (datasetVector != null) {
                datasetVector.close();
                datasetVector.dispose();
            }

            if (datasource != null) {
                datasource.close();
            }

            workspace.close();
            workspace.dispose();
        }
    }
}
