package com.company;

import com.company.helper.Helper;
import com.supermap.data.*;
import com.supermap.data.processing.*;
import com.supermap.data.processing.MaterialType;
import com.supermap.realspace.CacheFileType;

import java.util.HashMap;

public class OSGBCacheBuilderTest {

    private static final String datasourceFilePath = "D:\\SuperMap\\supermap-iobjectsjava-11.0.1-21420-98023-win-all\\SampleData\\CBDDataset\\CBD.udb";
    private static final String osgbCacheName = "CBDCache";

    private static final SteppedListener steppedListener = steppedEvent -> {
        System.out.println("模型缓存生成进度：" + steppedEvent.getPercent() + " ---- 剩余时间：" + Helper.getDate(steppedEvent.getRemainTime()));
    };

    public static void main(String[] args) {
        String runtimeFolderPath = System.getProperty("user.dir");
        String outputCacheFolderPath = runtimeFolderPath + "\\out";

        Helper.deleteFolder(outputCacheFolderPath + "\\" + osgbCacheName);

        DatasourceConnectionInfo datasourceConnectionInfo = new DatasourceConnectionInfo();
        datasourceConnectionInfo.setEngineType(EngineType.UDB);
        datasourceConnectionInfo.setServer(datasourceFilePath);
        datasourceConnectionInfo.setAlias("CBD");

        Workspace workspace = new Workspace();
        Datasources datasources = workspace.getDatasources();
        Datasource datasource = datasources.open(datasourceConnectionInfo);
        Datasets datasets = datasource.getDatasets();
        DatasetVector buildingDatasetVector = (DatasetVector) datasets.get("Building");

        HashMap<Integer, Double> simplifyPercent = new HashMap<>();
        simplifyPercent.put(1, 0.75);
        simplifyPercent.put(2, 0.5);
        simplifyPercent.put(3, 0.25);

        OSGBCacheBuilder osgbCacheBuilder = new OSGBCacheBuilder();
        osgbCacheBuilder.addSteppedListener(steppedListener);
        osgbCacheBuilder.setCacheVersion(S3MVersion.VERSION_30);
        osgbCacheBuilder.setDataset(buildingDatasetVector);
        osgbCacheBuilder.setFileType(CacheFileType.S3MB);
        osgbCacheBuilder.setAttributeExtentType(AttributeExtentType.S3MD);
        osgbCacheBuilder.setCompressedTextureType(CompressedTextureType.WEBP);
        osgbCacheBuilder.setLimitTextureSize(CacheLimitTextureSize.NoLimit);
        osgbCacheBuilder.setMaterialType(MaterialType.NORMAL);
        osgbCacheBuilder.setMultiTextureMode(MultiTextureMode.DOUBLECHANNEL);
        osgbCacheBuilder.setProcessType(ProcessFileType.NORMAL);
        osgbCacheBuilder.setSimplifyPercent(simplifyPercent);
        osgbCacheBuilder.setStorageType(StorageType.Original);
        osgbCacheBuilder.setTilePyramidSplitType(PyramidSplitType.Octree);
        osgbCacheBuilder.setLODSize(3);
        osgbCacheBuilder.setProcessThreadsCount(12);
        osgbCacheBuilder.setCalTangentAndBinormal(true);
        osgbCacheBuilder.setCalTilingTextureCoords(false);
        osgbCacheBuilder.setCombineRepeatTextures(true);
        osgbCacheBuilder.setHasOutLine(false);
        osgbCacheBuilder.setIsCalNormal(true);
        osgbCacheBuilder.setIsInstance(true);
        osgbCacheBuilder.setLogEnable(true);
        osgbCacheBuilder.setRemoveNormal(true);
        osgbCacheBuilder.setTextureSharing(false);
        osgbCacheBuilder.setOutputFolder(outputCacheFolderPath);
        osgbCacheBuilder.setCacheName(osgbCacheName);

        boolean built = false;
        try {
            built = osgbCacheBuilder.build();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            simplifyPercent.clear();

            osgbCacheBuilder.removeSteppedListener(steppedListener);
            osgbCacheBuilder.dispose();

            buildingDatasetVector.close();
            buildingDatasetVector.dispose();

            datasource.close();

            workspace.close();
            workspace.dispose();
        }

        System.out.println(built ? "模型缓存生成成功！" : "模型缓存生成失败！！！");
    }
}
