package com.company;

import com.supermap.data.*;
import com.supermap.data.processing.DataUnit;
import com.supermap.data.processing.ObliquePhotogrammetryBuilder;
import com.supermap.data.processing.ObliquePhotogrammetryUpdateParameter;
import com.supermap.data.processing.ObliqueProcessType;

import java.util.Date;

public class ObliquePhotogrammetryBuilderTest {

    private static final String originS3MFilePath = "D:\\SuperMap\\SuperMap SampleData\\区块八cs\\8cs\\Config-1\\Combine.scp";
    private static final String updateOSGBFilePath = "D:\\SuperMap\\SuperMap SampleData\\区块八cs\\Data1\\Config-1.scp";

    public static void main(String[] args) {
        ObliquePhotogrammetryUpdateParameter updateParameter = new ObliquePhotogrammetryUpdateParameter();

        updateParameter.setOriginSCPFile(originS3MFilePath);
        updateParameter.setUpdateSCPFile(updateOSGBFilePath);
        Point2Ds point2Ds = new Point2Ds();
        point2Ds.add(new Point2D(454096.77052722411, 4303569.7998921722));
        point2Ds.add(new Point2D(454096.77052722411, 4308835.2969624847));
        point2Ds.add(new Point2D(454177.28039966064, 4308835.2969624847));
        point2Ds.add(new Point2D(454177.28039966064, 4303569.7998921722));
        point2Ds.add(new Point2D(454096.77052722411, 4303569.7998921722));
        updateParameter.setUpdateRegions(new GeoRegion[]{new GeoRegion(point2Ds)});
        updateParameter.setDataUnit(DataUnit.Meter);
        updateParameter.setThreadCount(10);
        updateParameter.setTimeRecord(new Date(System.currentTimeMillis()));

        ObliquePhotogrammetryBuilder builder = new ObliquePhotogrammetryBuilder(new ObliqueProcessType[]{
                ObliqueProcessType.COMBINE_NODE
        });

        builder.addSteppedListener(steppedEvent -> System.out.println(steppedEvent.getPercent()));

        if (!builder.update(updateParameter)) {
            System.out.println("更新失败");
        } else {
            System.out.println("更新成功");
        }
    }
}
