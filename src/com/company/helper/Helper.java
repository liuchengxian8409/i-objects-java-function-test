package com.company.helper;

import java.io.File;

public final class Helper {

    public static void deleteFolder(String folderPath) {
        File file = new File(folderPath);

        if (file.exists()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File subFile : files) {
                    String subFileAbsolutePath = subFile.getAbsolutePath();
                    if (subFile.isDirectory()) {
                        deleteFolder(subFileAbsolutePath);
                    } else if (subFile.isFile()) {
                        subFile.delete();
                    }
                }
            }

            file.delete();
        }
    }

    public static String getDate(long second) {
        String date = "";

        if (second < 60) {
            date = second + " 秒";
        } else if (second > 60 && second < 3600) {
            long m = second / 60;
            long s = second % 60;
            date = m + " 分" + s + " 秒";
        } else {
            long h = second / 3600;
            long m = (second % 3600) / 60;
            long s = (second % 3600) % 60;
            date = h + " 小时" + m + " 分" + s + " 秒";
        }

        return date;
    }
}
