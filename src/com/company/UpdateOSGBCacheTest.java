package com.company;

import com.supermap.data.*;
import com.supermap.data.processing.*;
import com.supermap.data.processing.MaterialType;
import com.supermap.realspace.CacheFileType;

import java.util.ArrayList;
import java.util.HashMap;

public class UpdateOSGBCacheTest {

    private static final String workspaceFilePath = "C:\\Users\\LiuChengxian\\Downloads\\测试数据\\WorkSpace.smwu";
    private static final String updateDatasetVectorName = "RS3D_YQ_ROOM_1";
    private static final String cacheOutputFolderPath = "C:\\Users\\LiuChengxian\\Downloads\\RS3D_YQ_ROOM@DataSource1";
    private static final String cacheName = "RS3D_YQ_ROOM@DataSource1";

    public static void main(String[] args) {
        Workspace workspace = new Workspace();
        WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo();
        connectionInfo.setType(WorkspaceType.SMWU);
        connectionInfo.setServer(workspaceFilePath);
        workspace.open(connectionInfo);
        Datasource datasource = workspace.getDatasources().get(0);
        Dataset building = datasource.getDatasets().get(updateDatasetVectorName);

        OSGBCacheBuilder builder = new OSGBCacheBuilder();
        builder.setOutputFolder(cacheOutputFolderPath);
        builder.setCacheName(cacheName);
        builder.setCacheVersion(S3MVersion.VERSION_30);
        builder.setStorageType(StorageType.Compact);
        builder.setGlobeType(GlobeType.ELLIPSOID_WGS84);
        builder.setFileType(CacheFileType.S3MB);
        builder.setAttributeExtentType(AttributeExtentType.ATTRIBUTE);
        builder.setCompressedTextureType(CompressedTextureType.CRNDXT5);
        builder.setMaterialType(MaterialType.NORMAL);
        builder.setMultiTextureMode(MultiTextureMode.DOUBLECHANNEL);
        builder.setLimitTextureSize(CacheLimitTextureSize.LimitSize_4096);
        builder.setVertexWeightMode(VertexWeightMode.VWM_NONE);
        builder.setDataset((DatasetVector) building);
        builder.setObjectFiltrateThreshold(2);
        builder.setProcessThreadsCount(12);
        builder.setProcessType(ProcessFileType.NORMAL);
        builder.setTileWidth(152.87405657);
        builder.setIsInstance(true);
        builder.setPosition(new Point3D(114.17422572812856, 30.461084750676065, 0));
        HashMap<Integer, Double> mapPercent = new HashMap<>();
        mapPercent.put(0, 0.75);
        mapPercent.put(1, 0.5);
        mapPercent.put(2, 0.25);
        builder.setSimplifyPercent(mapPercent);
        builder.setLODSize(3);


        UpdateObjectsInfo updateObjectsInfo = new UpdateObjectsInfo();
        updateObjectsInfo.setDataset((DatasetVector) building);
        ArrayList<Integer> objectIDs = new ArrayList<>();
        int recordCount = ((DatasetVector) building).getRecordCount();
        for (int i = 0; i < recordCount; i++) {
            objectIDs.add(i + 1);
        }
        updateObjectsInfo.setObjectIDs(objectIDs);

        ArrayList<UpdateObjectsInfo> updateObjectsInfos = new ArrayList<>();
        updateObjectsInfos.add(updateObjectsInfo);
        builder.setUpdateObjectsInfoArray(updateObjectsInfos);

        boolean append = builder.append(UpdateType.ALL, true);

        System.out.println(append ? "更新三维缓存成功！" : "更新三维缓存失败！！！");
    }
}
