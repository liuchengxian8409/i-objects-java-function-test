package com.company;

import com.company.helper.Helper;
import com.supermap.data.SteppedListener;
import com.supermap.data.processing.ObliquePhotogrammetry3DModelTool;
import com.supermap.data.processing.S3MTo3DTilesParameters;

public class S3MTo3DTilesTest {

    private static final String scpFilePath = "D:\\SuperMap\\supermap-iobjectsjava-11.0.1-21420-98023-win-all\\SampleData\\S3M\\西城区建筑s3m\\西城区建筑s3m.scp";

    private static final SteppedListener steppedListener = steppedEvent -> {
        System.out.println("S3M 转换 3DTiles 进度：" + steppedEvent.getPercent() + " ---- 剩余时间：" + Helper.getDate(steppedEvent.getRemainTime()));
    };

    public static void main(String[] args) {

        String runtimeFolderPath = System.getProperty("user.dir");
        String outputFolderPath = runtimeFolderPath.concat("\\out\\3DTiles");

        Helper.deleteFolder(outputFolderPath);

        S3MTo3DTilesParameters s3MTo3DTilesParameters = new S3MTo3DTilesParameters(scpFilePath, outputFolderPath, 12);
        ObliquePhotogrammetry3DModelTool.addSteppedListener(steppedListener);
        boolean result = false;
        try {
            result = ObliquePhotogrammetry3DModelTool.s3mTo3DTiles(s3MTo3DTilesParameters);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ObliquePhotogrammetry3DModelTool.removeSteppedListener(steppedListener);
        }

        System.out.println(result ? "S3M 转 3DTiles 成功！" : "S3M 转 3DTiles 失败！！！");
    }
}
